package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * If you want to subscribe to available rooms, implement this interface and pass it to the appropriate subscribe method in {@link TwilioMessageHub}.
 */
public interface AvailableRoomsSubscription {

    /**
     * You'll keep getting room updates, until you unsubscribe.
     */
    void onUpdate(@NonNull List<AccessToken> onlineUsers);

    /**
     * Get's called if an error occurred.
     *
     * @param errorMessage the error that occurred.
     */
    void onError(@NonNull String errorMessage);
}
