package com.infinity.app.twilioapp.messaging;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.twilio.video.LocalMedia;
import com.twilio.video.Participant;
import com.twilio.video.Room;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;


/**
 * Used by {@link TwilioVideoService} to propagate room status changes to the message hub.
 */
final class RoomStatus {

    //What happened - each eventtype is expected to have a certain payload.
    @Status
    private final int status;
    //Potential payload
    @NonNull
    private final Room room;
    //Potential payload
    @Nullable
    private Participant participant;
    //Potential payload in case of joining a room
    @Nullable
    private LocalMedia localMedia;

    private RoomStatus(@Status int status, @NonNull Room room) {
        this.status = status;
        this.room = room;
    }

    int getStatus() {
        return status;
    }

    @NonNull
    Room getRoom() {
        return room;
    }

    @Nullable
    Participant getParticipant() {
        return participant;
    }

    @Nullable
    LocalMedia getLocalMedia() {
        return localMedia;
    }

    /**
     * Lets you compose an instance of {@link RoomStatus}.
     */
    static class Builder {

        private final RoomStatus roomStatus;

        Builder(@Status int status, @NonNull Room room) {
            roomStatus = new RoomStatus(status, room);
        }

        Builder setParticipant(@NonNull Participant participant) {
            roomStatus.participant = participant;
            return this;
        }

        /**
         * Setting a local media is required for the state {@link Status#CONNECTED_TO_ROOM}.
         */
        Builder setLocalMedia(@NonNull LocalMedia localMedia) {
            roomStatus.localMedia = localMedia;
            return this;
        }

        RoomStatus build() {
            //TODO consider enforcing the setting of certain variables for certain states (like localmedia for connected to Room state)
            return roomStatus;
        }
    }

    @Retention(SOURCE)
    @IntDef({CONNECTED_TO_ROOM, DISCONNECTED_FROM_ROOM, CONNECTION_FAILED, USER_JOINED, USER_LEFT})
    @interface Status {

    }

    static final int CONNECTED_TO_ROOM = 0;
    static final int DISCONNECTED_FROM_ROOM = 1;
    static final int CONNECTION_FAILED = 2;
    static final int USER_JOINED = 3;
    static final int USER_LEFT = 4;

}
