package com.infinity.app.twilioapp.view;

import android.util.Log;

import com.twilio.video.AudioTrack;
import com.twilio.video.Media;
import com.twilio.video.VideoTrack;

/**
 * Stub implementation for media events, so we don't have to override them all.
 */
abstract class DefaultMediaListener implements Media.Listener {

    private static final String TAG = DefaultMediaListener.class.getSimpleName();

    @Override
    public void onAudioTrackAdded(Media media, AudioTrack audioTrack) {
        Log.i(TAG, "Audio track has been added");
    }

    @Override
    public void onAudioTrackRemoved(Media media, AudioTrack audioTrack) {
        Log.i(TAG, "Audio track has been removed");
    }

    @Override
    public void onAudioTrackEnabled(Media media, AudioTrack audioTrack) {
        Log.i(TAG, "Audio track has been enabled");
    }

    @Override
    public void onAudioTrackDisabled(Media media, AudioTrack audioTrack) {
        Log.i(TAG, "Audio track has been disabled");
    }

    @Override
    public void onVideoTrackEnabled(Media media, VideoTrack videoTrack) {
        Log.i(TAG, "Video track has been enabled");
    }

    @Override
    public void onVideoTrackDisabled(Media media, VideoTrack videoTrack) {
        Log.i(TAG, "Video track has been disabled");
    }
}
