package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains base methods for dealing with subscriptions.
 */
abstract class SubscriptionHandler<T> {

    private List<T> subscribers;

    void subscribe(@Nullable T subscription) {
        if (subscription != null) {
            if (subscribers == null) {
                subscribers = new ArrayList<>();
            }
            if (!subscribers.contains(subscription)) {
                subscribers.add(subscription);
            }
        }
    }

    /**
     * @return how many subscribers are left in the given target list.
     */
    int unsubscribe(@Nullable T subscription) {
        int size = 0;
        if (subscribers != null) {
            if (subscription != null && subscribers.contains(subscription)) {
                subscribers.remove(subscription);
            }
            size = subscribers.size();
        }
        return size;
    }

    /**
     * If the list is empty it's actually immutable, it makes no practical difference, since you should not attempt to modify the list by adding or removing
     * either way.
     *
     * @return Unmodifiable list of subscribers. Do not attempt to modify the list by adding or removing entries.
     */
    @NonNull
    final List<T> getSubscribers() {
        return subscribers != null ? Collections.unmodifiableList(subscribers) : Collections.EMPTY_LIST;
    }
}
