package com.infinity.app.twilioapp.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.infinity.app.twilioapp.R;
import com.infinity.app.twilioapp.messaging.AccessToken;
import com.infinity.app.twilioapp.messaging.RoomStatusSubscription;
import com.infinity.app.twilioapp.messaging.TwilioMessageHub;
import com.twilio.video.CameraCapturer;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalMedia;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.Media;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.Map;

/**
 * Shows a call interface to the user. This is displayed when the user is currently in a call. It streams participant's video and audio feed.
 */
public final class CallActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 1;

    public static final String BUNDLE_TARGET_ROOM = "roomName";
    public static final String BUNDLE_OUTGOING_CALL = "outgoingCall";

    /**
     * Android application UI elements
     */
    private VideoView primaryVideoView;
    private VideoView thumbnailVideoView;
    private VideoView localVideoView;
    private FloatingActionButton localVideoActionFab;
    private FloatingActionButton micActionFab;
    private FloatingActionButton switchCameraActionFab;
    private LinearLayout statusBar;
    private TextView statusBarText;
    private ProgressBar statusBarSpinner;

    /** Twilio media related properties */
    private CameraCapturer cameraCapturer;
    private LocalMedia localMedia;
    private AudioManager audioManager;
    private LocalAudioTrack localAudioTrack;
    private LocalVideoTrack localVideoTrack;

    private String participantIdentity;
    private RoomStatusSubscription roomListenerSubscription;
    private boolean isOutgoingCall;
    // The room we were meant to connect to
    private AccessToken targetRoom;
    // The room we actually connected to
    private Room room;
    private boolean disconnectedFromOnDestroy;
    private int previousAudioMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        //Cache view references
        final FloatingActionButton callActionFab = (FloatingActionButton) findViewById(R.id.call_action_fab);
        callActionFab.setOnClickListener(this);
        localVideoActionFab = (FloatingActionButton) findViewById(R.id.local_video_action_fab);
        localVideoActionFab.setOnClickListener(this);
        switchCameraActionFab = (FloatingActionButton) findViewById(R.id.switch_camera_fab);
        switchCameraActionFab.setOnClickListener(this);
        micActionFab = (FloatingActionButton) findViewById(R.id.mute_action_fab);
        micActionFab.setOnClickListener(this);
        primaryVideoView = (VideoView) findViewById(R.id.primary_video_view);
        thumbnailVideoView = (VideoView) findViewById(R.id.thumbnail_video_view);
        statusBar = (LinearLayout) findViewById(R.id.status_bar);
        statusBarText = (TextView) findViewById(R.id.status_bar_text);
        statusBarSpinner = (ProgressBar) findViewById(R.id.status_bar_progress);
        //Enable changing the volume using the up/down keys during a conversation
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        //Needed for setting/abandoning audio focus during call
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        //We'll use this to listen for room updates
        final Bundle extras = getIntent().getExtras();
        isOutgoingCall = extras.getBoolean(BUNDLE_OUTGOING_CALL);
        targetRoom = extras.getParcelable(BUNDLE_TARGET_ROOM);
        roomListenerSubscription = new RoomListenerSubscription(targetRoom);

        if (!checkPermissionForCameraAndMicrophone()) {
            requestPermissionForCameraAndMicrophone();
        } else {
            onPermissionsGranted();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //If the local video track was removed when the app was put in the background, add it back.
        if (localMedia != null && localVideoTrack == null) {
            localVideoTrack = localMedia.addVideoTrack(true, cameraCapturer);
            localVideoTrack.addRenderer(localVideoView);
        }
    }

    @Override
    protected void onPause() {
        /**
         * Remove the local video track before going in the background. This ensures that the
         * camera can be used by other applications while this app is in the background.
         *
         * If this local video track is being shared in a Room, participants will be notified
         * that the track has been removed.
         */
        if (localMedia != null && localVideoTrack != null) {
            localMedia.removeVideoTrack(localVideoTrack);
            localVideoTrack = null;
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        disconnectedFromOnDestroy = true;
        //Unsubscribe from video call related updates
        TwilioMessageHub.getInstance().unsubscribe(roomListenerSubscription);
        if (room != null) {
            room.disconnect();
            room = null;
        }
        //Release the local media ensuring any memory allocated to audio or video is freed.
        if (localMedia != null) {
            localMedia.release();
            localMedia = null;
        }
        //Reconnect to our own room when leaving a call
        TwilioMessageHub.getInstance().onJoinOwnRoom(true);
        super.onDestroy();
    }

    private boolean checkPermissionForCameraAndMicrophone() {
        int resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return resultCamera == PackageManager.PERMISSION_GRANTED && resultMic == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForCameraAndMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat
                .shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(this, R.string.permissions_needed, Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat
                    .requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, CAMERA_MIC_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
            boolean cameraAndMicPermissionGranted = true;

            for (int grantResult : grantResults) {
                cameraAndMicPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
            }

            if (cameraAndMicPermissionGranted) {
                onPermissionsGranted();
            } else {
                Toast.makeText(this, R.string.camera_and_mic_permissions_needed, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * The user has granted all required permissions, now we can start setting up our call.
     */
    private void onPermissionsGranted() {
        //Request to join room, either due to an outgoing call or an incoming call
        final TwilioMessageHub messageHub = TwilioMessageHub.getInstance();
        messageHub.subscribe(roomListenerSubscription);
        if (messageHub.onJoinRoom(targetRoom)) {
            if (isOutgoingCall) {
                //Indicate to the user that we're trying to establish a call
                updateStatusBarText(getString(R.string.calling), true);
            }
        } else {
            //Show an error message
            updateStatusBarText(getString(R.string.outgoing_call_failed), false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mute_action_fab:
                if (localAudioTrack != null) {
                    boolean enable = !localAudioTrack.isEnabled();
                    localAudioTrack.enable(enable);
                    micActionFab.setImageDrawable(getVectorDrawable(enable ? R.drawable.ic_mic_on : R.drawable.ic_mic_off));
                }
                break;
            case R.id.local_video_action_fab:
                if (localVideoTrack != null) {
                    //Toogle showing/disabling video
                    boolean enable = !localVideoTrack.isEnabled();
                    localVideoTrack.enable(enable);
                    localVideoActionFab.setImageDrawable(getVectorDrawable(enable ? R.drawable.ic_videocam_on : R.drawable.ic_videocam_off));
                }
                break;
            case R.id.switch_camera_fab:
                if (cameraCapturer != null) {
                    CameraCapturer.CameraSource cameraSource = cameraCapturer.getCameraSource();
                    cameraCapturer.switchCamera();
                    if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
                        thumbnailVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
                    } else {
                        primaryVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
                    }
                }
                break;
            case R.id.call_action_fab:
                onBackPressed();
                finish();
                break;
        }
    }

    /**
     * @param drawableId the resource id of the drawable
     * @return the created {@link VectorDrawableCompat} instance.
     */
    private VectorDrawableCompat getVectorDrawable(@DrawableRes int drawableId) {
        return VectorDrawableCompat.create(getResources(), drawableId, getTheme());
    }

    /**
     * Initially the status bar is hidden.
     *
     * @param text        if the given text is null or empty, the status bar is hidden, if the text is not null or empty, the status bar shows.
     * @param showSpinner true if the spinner should be shown (if the text is not empty); false otherwise.
     */
    private void updateStatusBarText(@Nullable CharSequence text, boolean showSpinner) {
        if (TextUtils.isEmpty(text)) {
            statusBarText.setText("");
            statusBar.setVisibility(View.GONE);
        } else {
            statusBar.setVisibility(View.VISIBLE);
            statusBarText.setText(text);
            statusBarSpinner.setVisibility(showSpinner ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Sets up the {@link LocalMedia} instance that'll be used to stream video and audio.
     */
    private void createLocalMedia() {
        // Share your microphone
        localAudioTrack = localMedia.addAudioTrack(true);

        // Share your camera
        cameraCapturer = new CameraCapturer(this, getDefaultCamera());
        localVideoTrack = localMedia.addVideoTrack(true, cameraCapturer);
        primaryVideoView.setMirror(true);
        localVideoTrack.addRenderer(primaryVideoView);
        localVideoView = primaryVideoView;
    }

    /**
     * @return the default camera of the device.
     */
    static CameraCapturer.CameraSource getDefaultCamera() {
        Camera.CameraInfo info = new Camera.CameraInfo();

        for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return CameraCapturer.CameraSource.FRONT_CAMERA;
            }
        }
        return CameraCapturer.CameraSource.FRONT_CAMERA;
    }

    /**
     * Called when participant joins the room
     */
    private void addParticipant(Participant participant) {
        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
            //We only support one additional participant per room for now

            return;
        }
        participantIdentity = participant.getIdentity();
        updateStatusBarText(getString(R.string.call_participant_joined), false);

        // Add participant renderer
        if (!participant.getMedia().getVideoTracks().isEmpty()) {
            addParticipantVideo(participant.getMedia().getVideoTracks().get(0));
        }

        //Start listening for participant media events
        participant.getMedia().setListener(new MediaListener());
    }

    /**
     * Set primary view as renderer for participant video track
     */
    private void addParticipantVideo(VideoTrack videoTrack) {
        moveLocalVideoToThumbnailView();
        primaryVideoView.setMirror(false);
        videoTrack.addRenderer(primaryVideoView);
        requestPermissionForCameraAndMicrophone();

    }

    private void moveLocalVideoToThumbnailView() {
        if (thumbnailVideoView.getVisibility() == View.GONE) {
            thumbnailVideoView.setVisibility(View.VISIBLE);
            localVideoTrack.removeRenderer(primaryVideoView);
            localVideoTrack.addRenderer(thumbnailVideoView);
            localVideoView = thumbnailVideoView;
            thumbnailVideoView.setMirror(cameraCapturer.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }

    /**
     * Called when participant leaves the room
     */
    private void removeParticipant(Participant participant) {
        updateStatusBarText(getString(R.string.call_participant_left), false);
        if (!participant.getIdentity().equals(participantIdentity)) {
            return;
        }

        //Remove participant renderer
        if (!participant.getMedia().getVideoTracks().isEmpty()) {
            removeParticipantVideo(participant.getMedia().getVideoTracks().get(0));
        }
        participant.getMedia().setListener(null);
        moveLocalVideoToPrimaryView();
        finish();
    }

    private void removeParticipantVideo(VideoTrack videoTrack) {
        videoTrack.removeRenderer(primaryVideoView);
    }

    private void moveLocalVideoToPrimaryView() {
        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
            localVideoTrack.removeRenderer(thumbnailVideoView);
            thumbnailVideoView.setVisibility(View.GONE);
            localVideoTrack.addRenderer(primaryVideoView);
            localVideoView = primaryVideoView;
            primaryVideoView.setMirror(cameraCapturer.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }

    private void setAudioFocus(boolean focus) {
        if (focus) {
            previousAudioMode = audioManager.getMode();
            // Request audio focus before making any device switch.
            audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            /*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
             */
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        } else {
            audioManager.setMode(previousAudioMode);
            audioManager.abandonAudioFocus(null);
        }
    }

    /**
     * Once the user enters this dialog, we'll use this to listen to room-related events, such as users joining our room, leaving or us disconnecting from the
     * room.
     */
    private class RoomListenerSubscription extends DefaultRoomStatusSubscription {

        /**
         * @param roomFilter we filter all incoming notifications by room name, so we're not flooded with updates for rooms, that we don't care about.
         */
        private RoomListenerSubscription(@Nullable AccessToken roomFilter) {
            super(roomFilter);
        }

        @Override
        public void onConnected(@NonNull Room connectedTo, @NonNull LocalMedia roomsLocalMedia) {
            super.onConnected(connectedTo, roomsLocalMedia);
            if (room == null && localMedia == null) {
                room = connectedTo;
                //Update local media reference
                localMedia = roomsLocalMedia;
                //We're now connected, so we can kick our setup routine in motion
                updateStatusBarText("", false);
                //Initialize our local media instance
                createLocalMedia();
                for (Map.Entry<String, Participant> entry : connectedTo.getParticipants().entrySet()) {
                    addParticipant(entry.getValue());
                    //Right now we only support one other participant in a call
                    break;
                }
            }
        }

        @Override
        public void onConnectFailed() {
            super.onConnectFailed();
            updateStatusBarText(getString(R.string.outgoing_call_failed), false);
        }

        @Override
        public void onDisconnected() {
            super.onDisconnected();
            CallActivity.this.room = null;
            // Only reinitialize the UI if disconnect was not called from onDestroy()
            if (!disconnectedFromOnDestroy) {
                setAudioFocus(false);
                //TODO we may have to reset the UI here
                moveLocalVideoToPrimaryView();
            }
        }

        @Override
        public void someoneJoined(@NonNull Participant participant) {
            super.someoneJoined(participant);
            updateStatusBarText("", false);
            addParticipant(participant);
        }

        @Override
        public void someoneLeft(@NonNull Participant participant) {
            super.someoneLeft(participant);
            updateStatusBarText("", false);
            removeParticipant(participant);
        }

        @Override
        public boolean shouldSilenceIncomingCallNotifications() {
            /**
             * When we're already in the call dialog we don't want to see incoming call notifications.
             */
            return true;
        }
    }

    /**
     * Listens to media events from a given participant.
     */
    private class MediaListener extends DefaultMediaListener {

        @Override
        public void onVideoTrackAdded(Media media, VideoTrack videoTrack) {
            addParticipantVideo(videoTrack);
        }

        @Override
        public void onVideoTrackRemoved(Media media, VideoTrack videoTrack) {
            removeParticipantVideo(videoTrack);

        }
    }
}