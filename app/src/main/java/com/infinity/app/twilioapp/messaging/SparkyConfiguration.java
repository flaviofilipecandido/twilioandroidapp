package com.infinity.app.twilioapp.messaging;

import android.content.Context;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Default sparky configuration for the productive environment.
 * SparkyConfiguration#TEST_LOCAL}.
 */
public final class SparkyConfiguration {

    private static final String TAG = "SparkyConfiguration";
    private static final String PATTERN_ERROR_MESSAGE = "%s - %s";
    @ConfigurationType
    private final int configurationType;

    public SparkyConfiguration(@ConfigurationType int configurationType) {
        this.configurationType = configurationType;
    }

    /**
     * Check the available target paths to know which other parameters are required when using them and/or which JSON objects are expected/accepted.
     *
     * @return the target path for the given {@link TargetPath}.
     * @throws IllegalArgumentException if the configuration type has not been set.
     */
    String getPath(@TargetPath int targetPath) {
        String result;
        switch (targetPath) {
            case AUTHENTICATE:
                result = "/token";
                break;
            case UNREGISTER:
                result = "/unregister";
                break;
            case USER_LIST:
                result = "/users";
                break;
            default:
                throw new IllegalArgumentException(String.format(PATTERN_ERROR_MESSAGE, TAG, "Invalid target path " + targetPath));

        }
        return result;
    }

    /**
     * @return sparky's server URL based on the configuration type.
     * @throws IllegalArgumentException if the configuration type has not been set.
     */
    String getServerUrl(Context context) {
        String result;
        switch (configurationType) {
            case PRODUCTIVE:
                result = "http://twiliourlhelper.azurewebsites.net/api/twilio";
                break;
            case TEST_LOCAL:
                //Loopback to localhost of development machine in case of using an emulator
                result = "http://10.0.2.2:4567";
                break;
            default:
                throw new IllegalArgumentException(String.format(PATTERN_ERROR_MESSAGE, TAG, "Invalid configuration type" + configurationType));
        }
        return result;
    }

    /**
     * @return the {@link AccessToken} TTL based on the current configuration.
     * @throws IllegalArgumentException if the configuration type has not been set.
     */
    long getTokenTTL() {
        long result;
        switch (configurationType) {
            case PRODUCTIVE:
                //1 hour for production
                result = 3600000;
                break;
            case TEST_LOCAL:
                //We take a shorter time span in testing so we can test re-obtaining tokens - 5 minutes
                result = 300000;
                break;
            default:
                throw new IllegalArgumentException(String.format(PATTERN_ERROR_MESSAGE, TAG, "Invalid configuration type" + configurationType));
        }
        return result;
    }

    @Retention(SOURCE)
    @IntDef({USER_LIST, UNREGISTER, AUTHENTICATE})
    public @interface TargetPath {

    }

    /** Use this to obtain a list of all the users from your account that are currently online from Sparky */
    public static final int USER_LIST = 0;
    /** Unregister from Sparky */
    public static final int UNREGISTER = 1;
    /** Sparky will provide you with an authentication key that you can use to authenticate with Twilio */
    public static final int AUTHENTICATE = 2;

    @Retention(SOURCE)
    @IntDef({PRODUCTIVE, TEST_LOCAL})
    public @interface ConfigurationType {

    }

    /**
     * Productive, live environment as used by customers.
     */
    public static final int PRODUCTIVE = 0;
    /**
     * Use this if sparky is running on your local machine and you'd like to test against that. This is recommended to test changes made to sparky without
     * deploying them to prod yet.
     */
    public static final int TEST_LOCAL = 1;

}
