package com.infinity.app.twilioapp.messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.infinity.app.twilioapp.IncomingCall;
import com.infinity.app.twilioapp.R;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalMedia;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.RoomState;
import com.twilio.video.TwilioException;
import com.twilio.video.VideoClient;

import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_OUTGOING_CALL;
import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_TARGET_ROOM;

/**
 * Service that runs in the background and manages the interaction with Twilio.
 */
public final class TwilioVideoService extends Service implements SparkyClient.AuthCallback, VideoService {

    private static final String TAG = TwilioVideoService.class.getSimpleName();
    private static final String INTENT_STOP_SERVICE = "StopTwilioService";
    private static final String INTENT_CANCEL_INCOMING_INVITE = "CancelIncomingInvite";
    private static final int NOTIFICATION_ID = 1;
    @NonNull
    private SparkyClient sparkyClient;
    @NonNull
    private Context applicationContext;
    //A chat room that people can join.
    private Room currentRoom;
    //Required to stream video and audio.
    private LocalMedia localMedia;
    //This will be set if a new room should be joined. We keep it here until the old room is disconnected and then reconnect.
    private JoinRoomRequest nextRoomToJoin;
    //Listens for requests to stop the twilio service
    private BroadcastReceiver receiver;
    // Cached access token from sparky's authentication response
    private AccessToken accessToken;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "has been created.");
        this.applicationContext = getApplicationContext();
        //Listen to broadcasts send to us
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent == null || intent.getAction() == null ? "" : intent.getAction();
                switch (action) {
                    case INTENT_STOP_SERVICE:
                        endTwilioSession();
                        break;
                    case ConnectivityManager.CONNECTIVITY_ACTION:
                        onNetworkConnectivityChanged();
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_STOP_SERVICE);
        filter.addAction(INTENT_CANCEL_INCOMING_INVITE);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
    }

    /**
     * Monitors Network activity and reacts accordingly.
     */
    private void onNetworkConnectivityChanged() {
        ConnectivityManager manager = (ConnectivityManager) applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            Log.i(TAG, "the user connected to the internet");
            startTwilioSession();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        endTwilioSession();
        try {
            unregisterReceiver(receiver);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Receiver was not registered in the first place.", e);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            startTwilioSession();
        }
        return Service.START_STICKY;
    }

    /**
     * Call this to trigger a twilio session. We'll obtain a token if there is an internet connection and we'll register a network receiver in case there
     * isn't, so we can obtain an authentication token as soon as we have a connection.
     * s
     */
    public void startTwilioSession() {
        //First make sure we don't already have an ongoing session
        if (isSparkyInitialized()) {
            //This is relevant if the internet connectivity changes, so we may have to re-authenticate with Twilio
            Log.i(TAG, "was running already, re-authenticating to ensure our token is valid.");
            sparkyClient.authenticate(applicationContext, this, true);
            //TODO check if this is a problem when in a call (though either way when we lose internet we'd drop out of the call)
        } else {
            Log.i(TAG, "Starting twilio Session ...");
            TwilioMessageHub.getInstance().registerTwilioService(this);
            //Create a sparky client instance with the desired configuration
            sparkyClient = new SparkyClient(new SparkyConfiguration(SparkyConfiguration.PRODUCTIVE), TwilioUser.toAccessToken());
            sparkyClient.authenticate(applicationContext, this, true);
        }
    }

    @Override
    public void joinOwnRoom(boolean disconnectFirst) {
        if (isSparkyInitialized()) {
            Log.i(TAG, "Received request to join own room");
            onJoinRoom(new JoinRoomRequest(sparkyClient.getAccessToken(), disconnectFirst));
        }
    }

    @Override
    public void onJoinRoom(@NonNull JoinRoomRequest event) {
        if (isSparkyInitialized()) {
            nextRoomToJoin = event;
            boolean joinNextRoom = false;
            if (isRoomConnected(currentRoom)) {
                if (event.isDisconnectFirst()) {
                    //Disconnect from our current room first, in our disconnect callback we'll join the new room
                    Log.i(TAG, "Received request to disconnect from room before connecting to the next one");
                    disconnect();
                    joinNextRoom = true;

                } else if (isSameAsOldroom(event.getRoomName(), currentRoom)) {
                    //Check if it's the same room that we're already in and if our room is still connected
                    Log.i(TAG, "We're already in the requested room and connected, informing subscribers");
                    //Same room, we'll just stay where we are
                    nextRoomToJoin = null;
                    fireOnRoomConnected(localMedia, currentRoom);
                } else {
                    Log.i(TAG, String.format("Current room %s is connected, triggering disconnect", currentRoom.getName()));
                    //Disconnect from our current room first, in our disconnect callback we'll join the new room
                    disconnect();
                    joinNextRoom = true;
                }
            } else {
                //We're not in any room, so we can connect straight away
                Log.i(TAG, "We're currently not in any room, so we'll connect straight to the new room");
                joinNextRoom = true;
            }

            if (joinNextRoom) {
                joinNextRoom(event);
            }
        }
    }

    /**
     * @return true if the given room is not null and we're currently connected to it.
     */
    private static boolean isRoomConnected(@Nullable Room room) {
        return room != null && RoomState.CONNECTED == room.getState();
    }

    private static boolean isSameAsOldroom(@Nullable String newRoom, Room oldRoom) {
        boolean result = false;
        if (!TextUtils.isEmpty(newRoom)) {
            final String oldRoomName = oldRoom != null ? oldRoom.getName() : "";
            result = oldRoomName.equals(newRoom);
        }
        return result;
    }

    /**
     * Joins a room. Seems like you can only join one room at a time, so this may be called multiple times - basically any time you'd like to join another
     * room. If the given room name is not null or empty, any previous room you joined will be disconnected and an attempt will be made to join the given room.
     *
     * @param request request containing the room you'd like to join.
     */
    private void joinNextRoom(@NonNull JoinRoomRequest request) {
        //The room as changed - so we join the new room
        if (accessToken != null) {
            Log.i(TAG, "joinNextRoom (creating localmedia and room) " + request.getRoomName());
            localMedia = LocalMedia.create(applicationContext);
            ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken.getToken()).roomName(request.getRoomName()).localMedia(localMedia).build();

            currentRoom = VideoClient.connect(applicationContext, connectOptions, new ChatServiceRoomListener());
        } else {
            Log.e(TAG, "Failed to joinNextRoom, we don't have a valid access token");
        }
    }

    @Override
    public void requestRoomUpdate() {
        if (currentRoom != null && RoomState.CONNECTED == currentRoom.getState()) {
            //We're in a room right now, thus we'll trigger a room connected event
            fireOnRoomConnected(localMedia, currentRoom);
        }
    }

    /**
     * Disconnects from the current room and cleans up the local media instance.
     */
    private void disconnect() {
        if (currentRoom != null) {
            currentRoom.disconnect();
            currentRoom = null;
            Log.i(TAG, "Disconnected from previous room");
        }
        if (localMedia != null) {
            localMedia.release();
            localMedia = null;
            Log.i(TAG, "Released Localmedia instance");
        }
    }

    @Override
    public void getUsers(@NonNull final SparkyClient.UserListCallback callback) {
        /*
        TODO this should work differently: If sparky is not available we should try to re-authenticate (without having to check for null everywhere)
        and then we should react to the authentication successful request by executing what we need to do - like request users
         */
        if (isSparkyInitialized()) {
            sparkyClient.getUsers(applicationContext, callback);
        }
    }

    /**
     * Call this to end a twilio session - calling this on logout would be a good idea, so the user's twilio session does not carry on, despite the user being
     * logged out. The call is symmetric with {@link TwilioVideoService#startTwilioSession}.
     */
    public void endTwilioSession() {
        TwilioMessageHub.getInstance().unregisterTwilioService();
        disconnect();
        if (isSparkyInitialized()) {
            sparkyClient.unregisterFromServer(applicationContext);
            sparkyClient = null;
        }
        stopSelf();
        Log.i(TAG, "Ended twilio session");
    }

    @Override
    public void onAuthenticated(AccessToken token, Context context) {
        Log.i(TAG, "Successfully authenticated");
        this.accessToken = token;
        //If we were re-authenticated due to expired token, we'll check the backlog first
        JoinRoomRequest request = nextRoomToJoin;
        if (request == null) {
            //There are no pending requests to join a room - so we'll create a default login
            request = new JoinRoomRequest(token);
        }
        onJoinRoom(request);
    }

    @Override
    public void onAuthenticationFailed() {
        Log.w(TAG, "Failed to authenticate user towards Twilio");
        Toast.makeText(this, R.string.authentication_failed, Toast.LENGTH_LONG).show();
    }

    private void showIncomingCallNotification(@NonNull String newParticipant) {
        if (isSparkyInitialized()) {
            final Intent intent = new Intent(this, IncomingCall.class);
            intent.putExtra(BUNDLE_OUTGOING_CALL, false);
            intent.putExtra(BUNDLE_TARGET_ROOM, sparkyClient.getAccessToken());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            final NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_call_white_24px)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(String.format(getString(R.string.incoming_call_from), newParticipant))
                    .setAutoCancel(true)
                    .setSound(alarmSound)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notification.setCategory(Notification.CATEGORY_CALL);
            }
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(NOTIFICATION_ID, notification.build());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Sets up the twilio sdk and registers the user for twilio call receival, provided the feature is enabled. If the feature is disabled (via company
     * setting) it won't be started.
     */
    public static void startTwilioService(@NonNull Context context) {
        //Start Twilio Service
        Log.i(TAG, "Attempting to start...");
        Intent intent = new Intent(context, TwilioVideoService.class);
        context.startService(intent);
    }

    /**
     * Fires a broadcast that requests a stopping of the twilio service and tears down the twilio SDK setup, provided the feature is enabled.
     */
    public static void stopTwilioService(@NonNull Context context) {
        Intent intent = new Intent(INTENT_STOP_SERVICE);
        context.sendBroadcast(intent);
    }

    /**
     * @param participant the participant to compare to ourselves.
     * @return true if the given participant matches our own user; false otherwise.
     */
    private boolean isThisMe(@Nullable Participant participant) {
        boolean result = false;
        if (participant != null && isSparkyInitialized()) {
            final AccessToken token = sparkyClient.getAccessToken();
            if (token != null) {
                //If the give participant is us, we'd have the same caller id/identity
                result = token.getCallerId().equals(participant.getIdentity());
            }
        }
        return result;
    }

    /**
     * Listens to all relevant room events.
     */
    private class ChatServiceRoomListener implements Room.Listener {

        @Override
        public void onParticipantConnected(Room room, Participant participant) {
            Log.i(TAG, String.format("Participant %1$s connected to room %2$s", participant.getIdentity(), room.getName()));
            if (participant != null && !isThisMe(participant)) {
                final TwilioMessageHub hub = TwilioMessageHub.getInstance();
                if (hub.shouldSilenceIncomingCallNotifications()) {
                    //In case we're already on the call dialog, someone new joined
                    final RoomStatus status = new RoomStatus.Builder(RoomStatus.USER_JOINED, room).setParticipant(participant).build();
                    hub.notifyRoomStatusChanged(status);
                } else {
                    //Someone connected to our room while we're not in the call dialog, show a message that a call is incoming
                    showIncomingCallNotification(participant.getIdentity());
                }
            }
        }

        @Override
        public void onParticipantDisconnected(Room room, Participant participant) {
            Log.i(TAG, String.format("Participant %1$s disconnected from room %2$s", participant.getIdentity(), room.getName()));
            if (participant != null && !isThisMe(participant)) {
                //Someone left our room, propagate the event so if the user is viewing the app he'll know that the person he was talking to left
                final RoomStatus status = new RoomStatus.Builder(RoomStatus.USER_LEFT, room).setParticipant(participant).build();
                TwilioMessageHub.getInstance().notifyRoomStatusChanged(status);
            }
        }

        @Override
        public void onRecordingStarted(Room room) {

        }

        @Override
        public void onRecordingStopped(Room room) {

        }

        @Override
        public void onDisconnected(Room room, TwilioException e) {
            final String message = "Disconnected from room " + room.getName();
            if (e == null) {
                Log.i(TAG, message);
            } else {
                Log.e(TAG, message, e);
            }
            final RoomStatus status = new RoomStatus.Builder(RoomStatus.DISCONNECTED_FROM_ROOM, room).build();
            TwilioMessageHub.getInstance().notifyRoomStatusChanged(status);
        }

        @Override
        public void onConnected(Room room) {

            Log.i(TAG, "onConnected to room " + room.getName());
            //We're done, so remove the room from the backlog
            nextRoomToJoin = null;
            fireOnRoomConnected(localMedia, currentRoom);
        }

        @Override
        public void onConnectFailure(Room room, TwilioException e) {
            Log.e(TAG, "Failed to connect to room " + room.getName(), e);
            //We'll leave the room to join in the backlog and attempt re-authentication before trying to join again
            if (isSparkyInitialized()) {
                sparkyClient.authenticate(applicationContext, TwilioVideoService.this, true);
            }
            TwilioMessageHub.getInstance().notifyRoomStatusChanged(new RoomStatus.Builder(RoomStatus.CONNECTION_FAILED, room).build());
        }
    }

    /**
     * Sends a notification to all subscribers if we connected to the given room.
     */
    private static void fireOnRoomConnected(@NonNull LocalMedia localMedia, @NonNull Room room) {
        final RoomStatus status = new RoomStatus.Builder(RoomStatus.CONNECTED_TO_ROOM, room).setLocalMedia(localMedia).build();
        TwilioMessageHub.getInstance().notifyRoomStatusChanged(status);
    }

    /**
     * @return true if sparky has been properly setup and is available for use; false otherwise.
     */
    private boolean isSparkyInitialized() {
        return sparkyClient != null;
    }
}
