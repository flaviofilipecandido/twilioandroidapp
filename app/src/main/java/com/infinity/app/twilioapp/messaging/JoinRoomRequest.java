package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Event created to pass around as a join-room-request.
 */
public final class JoinRoomRequest {

    private final String roomName;
    private final boolean disconnectFirst;

    JoinRoomRequest(@NonNull AccessToken token, boolean disconnectFirst) {
        this.roomName = token.getCallerId();
        this.disconnectFirst = disconnectFirst;
    }

    JoinRoomRequest(@NonNull AccessToken token) {
        this(token, false);
    }

    /**
     * @return true if we should disconnect before executing the room request; false otherwise.
     */
    boolean isDisconnectFirst() {
        return disconnectFirst;
    }

    @Nullable
    String getRoomName() {
        return roomName;
    }
}
