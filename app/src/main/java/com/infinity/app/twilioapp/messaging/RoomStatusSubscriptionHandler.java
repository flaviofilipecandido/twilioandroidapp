package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Notifies subscribers once a room's status has been changed, i.e. when users joined, left etc.
 */
final class RoomStatusSubscriptionHandler extends SubscriptionHandler<RoomStatusSubscription> {

    private final MessageHub messageHub;

    RoomStatusSubscriptionHandler(MessageHub hub) {
        messageHub = hub;
    }

    @Override
    void subscribe(@Nullable RoomStatusSubscription subscription) {
        super.subscribe(subscription);
        if (subscription != null) {
            //Immediately inform the subscriber which room he is in
            if (messageHub.hasVideoService()) {
                messageHub.getVideoService().requestRoomUpdate();
            }
        }
    }

    /**
     * @param update processes the update and distributes it to it's subscribers based on the subscriber's room name filters.
     */
    void process(@NonNull final RoomStatus update) {
        switch (update.getStatus()) {
            case RoomStatus.CONNECTED_TO_ROOM:
                notifySubscribers(new Action() {
                    @Override
                    public void execute(@NonNull RoomStatusSubscription subscriber) {
                        subscriber.onConnected(update.getRoom(), update.getLocalMedia());
                    }
                }, update);
                break;
            case RoomStatus.DISCONNECTED_FROM_ROOM:
                notifySubscribers(new Action() {
                    @Override
                    public void execute(@NonNull RoomStatusSubscription subscriber) {
                        subscriber.onDisconnected();
                    }
                }, update);
                break;
            case RoomStatus.CONNECTION_FAILED:
                notifySubscribers(new Action() {
                    @Override
                    public void execute(@NonNull RoomStatusSubscription subscriber) {
                        subscriber.onConnectFailed();
                    }
                }, update);
                break;
            case RoomStatus.USER_JOINED:
                notifySubscribers(new Action() {
                    @Override
                    public void execute(@NonNull RoomStatusSubscription subscriber) {
                        subscriber.someoneJoined(update.getParticipant());
                    }
                }, update);
                break;
            case RoomStatus.USER_LEFT:
                notifySubscribers(new Action() {
                    @Override
                    public void execute(@NonNull RoomStatusSubscription subscriber) {
                        subscriber.someoneLeft(update.getParticipant());
                    }
                }, update);
                break;
            default:
                //TODO we could inform the user here, but really - what should we tell him? Something like: We got an update but we don't know what it is xD ?
        }
    }

    private void notifySubscribers(@NonNull Action action, @NonNull RoomStatus update) {
        for (RoomStatusSubscription subscriber : getSubscribers()) {
            if (subscriber != null && subscriber.doesRoomMatch(update)) {
                //We'll only execute the actions if the room filter is a match
                action.execute(subscriber);
            }
        }
    }

    /**
     * @return true if at least one of the subscribers require's silencing of incoming callse; false otherwise
     */
    boolean shouldSilenceIncomingCalls() {
        for (RoomStatusSubscription subscriber : getSubscribers()) {
            if (subscriber.shouldSilenceIncomingCallNotifications()) {
                return true;
            }
        }
        return false;
    }

    private interface Action {

        void execute(@NonNull RoomStatusSubscription subscriber);
    }
}
