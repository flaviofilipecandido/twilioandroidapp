package com.infinity.app.twilioapp.messaging;

import android.support.annotation.Nullable;

interface MessageHub {

    boolean hasVideoService();

    @Nullable
    VideoService getVideoService();

    /**
     * Calling this method causes us to join our own room again so we can wait for incoming calls.
     *
     * @param disconnectFirst true if we should disconnect from the current room first; false otherwise.
     */
    void onJoinOwnRoom(boolean disconnectFirst);

    /**
     * @return true if incoming calls should be silenced; false otherwise.
     */
    boolean shouldSilenceIncomingCallNotifications();
}
