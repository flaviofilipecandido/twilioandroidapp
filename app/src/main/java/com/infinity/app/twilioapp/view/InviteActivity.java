package com.infinity.app.twilioapp.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.infinity.app.twilioapp.PrefUtils;
import com.infinity.app.twilioapp.R;
import com.infinity.app.twilioapp.Session;
import com.infinity.app.twilioapp.firebase.Config;
import com.infinity.app.twilioapp.firebase.NotificationUtils;
import com.infinity.app.twilioapp.messaging.AccessToken;
import com.infinity.app.twilioapp.messaging.AvailableRoomsSubscription;
import com.infinity.app.twilioapp.messaging.TwilioMessageHub;
import com.twilio.video.LocalMedia;
import com.twilio.video.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_OUTGOING_CALL;
import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_TARGET_ROOM;

/**
 * Activity that shows a list of available users, that the user may call. Picking a user results in that user being called.
 */
public final class InviteActivity extends AppCompatActivity {

    private static final String TAG = InviteActivity.class.getSimpleName();
    private AccessTokenAdapter adapter;
    private View noOnlineContactsTextView;
    private TextView currentRoomView;
    private AvailableRoomsSubscription availableRoomsSubscription;
    //Mostly relevant to monitor the status of our own room
    private RoomStatusSubscription roomStatusSubscription;
    List<AccessToken> mlist = new ArrayList<>();
    private static String url = "http://twiliourlhelper.azurewebsites.net/video/contacts.json";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private RecyclerView recyclerView;
    private AccessToken accessToken;
    Button btn_logout;
    Intent in;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        btn_logout = (Button) findViewById(R.id.btn_logout);

       // adapter = new AccessTokenAdapter();
        availableRoomsSubscription = new RoomSubscription();
        roomStatusSubscription = new RoomStatusSubscription(null);
        noOnlineContactsTextView = findViewById(R.id.label_no_online_contacts);
        //TODO we may remove this, since it's pretty much just a crutch for testing so we know we're in some kind of room
        currentRoomView = (TextView) findViewById(R.id.current_room);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerView.setAdapter(adapter);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);


                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        new GetPersons().execute();
       /* List<AccessToken> mlist=new ArrayList<>();
        AccessToken  toekn=new AccessToken("lrnovo124124123","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJpc3MiOiJTSzcwNjRjMzEyNTAwZDQwYzJjZTBhMzA5ODk0ZGM1NWZiIiwiZXhwIjoxNDk2NDc1MzA5LCJqdGkiOiJTSzcwNjRjMzEyNTAwZDQwYzJjZTBhMzA5ODk0ZGM1NWZiLTE0OTY0NzE3MDkiLCJzdWIiOiJBQzZiMmY3ZGEzZDk2YTY5ZTRiNTY1MmY3YjE5OGE0ODg5IiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoibHJub3ZvMTI0MTI0MTIzQGdtYWlsLmNvbSIsInZpZGVvIjp7InJvb20iOiJscm5vdm8xMjQxMjQxMjMifX19.CaEGkeAwbwwoTwsUSxj4TcL6UH4IKiUmfnVKnBKLD14","lrnovo124124123","lrnovo124124123@gmail.com","lrnovo124124123");
        mlist.add(toekn);
        adapter.updateEntries(mlist);*/
    }

    public void logout(View view) {
        PrefUtils.removeFromPrefs(InviteActivity.this, "item");
        Intent i = new Intent(InviteActivity.this, Session.class);
        startActivity(i);
        //onDestroy();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Subscribe for user-list updates
        TwilioMessageHub.getInstance().subscribe(availableRoomsSubscription);
        TwilioMessageHub.getInstance().subscribe(roomStatusSubscription);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Unsubscribe from user-list updates
        TwilioMessageHub.getInstance().unsubscribe(availableRoomsSubscription);
        TwilioMessageHub.getInstance().unsubscribe(roomStatusSubscription);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        currentRoomView = null;
        noOnlineContactsTextView = null;
        adapter = null;
    }

    /**
     * Recyclerview adapter for item rows that the user can interact with.
     */
    private static class AccessTokenAdapter extends RecyclerView.Adapter<AccessTokenAdapter.ViewHolder> implements OnItemClickedListener {

        private List<AccessToken> mlist = new ArrayList<>();

        public AccessTokenAdapter(GetPersons getPersons, List<AccessToken> mlists) {
            mlist = mlists;
            notifyDataSetChanged();
        }

        private void updateEntries(@NonNull List<AccessToken> newEntries) {
            mlist = newEntries;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.invite_item, parent, false), this);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final AccessToken onlineUser = mlist.get(position);
            //final String roomName = AccessToken.getRoomName(onlineUser);
            holder.name.setText(onlineUser.getmName());
            holder.itemView.setTag(onlineUser);
            //For now we got no access to the photos, so use the "textual photo"
            holder.photoTextual.setText(onlineUser.getmId());
            holder.email.setText(onlineUser.getmEmail());
        }

        /**
         * @param fullName the full name - could be something like "Patrick Jane"
         * @return in the case of the given example the result would be "PJ" - thus the first letters would be capitalized.
         */
        @NonNull
        private static CharSequence getNameLabel(@Nullable String fullName) {
            StringBuilder result = new StringBuilder(2);
            String[] parts = fullName != null ? fullName.split(" ") : new String[0];

            for (int i = 0, size = parts.length; i < size; i++) {
                if (!parts[i].isEmpty()) {
                    result.append(String.valueOf(parts[i].charAt(0)).toUpperCase());
                }
            }
            return result.toString();
        }

        @Override
        public void onListItemClicked(@NonNull AccessToken onlineUser, @NonNull Context context) {

            final Intent intent = new Intent(context, CallActivity.class);
            final Bundle bundle = new Bundle();
            bundle.putBoolean(BUNDLE_OUTGOING_CALL, true);
            bundle.putParcelable(BUNDLE_TARGET_ROOM, onlineUser);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        @Override
        public int getItemCount() {
            return mlist.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            //TODO currently not in use but will be when we get a hold of user images
            private final ImageView photoImage;
            private final TextView photoTextual;
            private final OnItemClickedListener onItemClicked;
            private final TextView name;
            private final TextView email;
            private final View itemView;

            public ViewHolder(View view, OnItemClickedListener onClick) {
                super(view);
                itemView = view;
                itemView.setOnClickListener(this);
                onItemClicked = onClick;
                photoImage = (ImageView) view.findViewById(R.id.photo_image);
                photoTextual = (TextView) view.findViewById(R.id.photo_text);
                name = (TextView) view.findViewById(R.id.name);
                email = (TextView) view.findViewById(R.id.email);
            }

            @Override
            public void onClick(View view) {
                final Object tag = view != null ? view.getTag() : null;
                if (onItemClicked != null && tag != null) {
                    onItemClicked.onListItemClicked((AccessToken) tag, view.getContext());
                }
            }
        }
    }

    private class RoomSubscription implements AvailableRoomsSubscription {

        private RoomSubscription() {
        }

        @Override
        public void onUpdate(@NonNull List<AccessToken> onlineUsers) {
            updateAdapter(onlineUsers);
            if (noOnlineContactsTextView != null) {
                noOnlineContactsTextView.setVisibility(onlineUsers.isEmpty() ? View.VISIBLE : View.GONE);
            }
        }

        private void updateAdapter(List<AccessToken> users) {
            if (adapter != null) {
                adapter.updateEntries(users);
            } else {
                Log.w(TAG, "Failed to update userlist - adapter is null.");
            }
        }

        @Override
        public void onError(@NonNull String errorMessage) {
            //TODO show message to the user either as toast or embedded in the dialog
        }
    }

    private class RoomStatusSubscription extends DefaultRoomStatusSubscription {

        /**
         * @param roomFilter we filter all incoming notifications by room name, so we're not flooded with updates for rooms, that we don't care about.
         */
        protected RoomStatusSubscription(@Nullable AccessToken roomFilter) {
            super(roomFilter);
        }

        @Override
        public void onConnected(@NonNull Room connectedTo, @NonNull LocalMedia roomsLocalMedia) {
            super.onConnected(connectedTo, roomsLocalMedia);
            currentRoomView.setText(connectedTo.getName());
        }

        @Override
        public void onDisconnected() {
            super.onDisconnected();
            currentRoomView.setText(R.string.none);
        }

        @Override
        protected boolean doesRoomMatch(@Nullable String otherRoomName) {
            //We just care what room we're in, not to whom the room actually belongs
            return true;
        }
    }

    private interface OnItemClickedListener {

        void onListItemClicked(@NonNull AccessToken onlineUser, @NonNull Context context);
    }

    private class GetPersons extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray category = jsonObj.getJSONArray("persons");

                    // looping through All Contacts
                    for (int i = 0; i < category.length(); i++) {
                        JSONObject c = category.getJSONObject(i);

                        AccessToken bean = new AccessToken("","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJpc3MiOiJTSzcwNjRjMzEyNTAwZDQwYzJjZTBhMzA5ODk0ZGM1NWZiIiwiZXhwIjoxNDk2NDc1MzA5LCJqdGkiOiJTSzcwNjRjMzEyNTAwZDQwYzJjZTBhMzA5ODk0ZGM1NWZiLTE0OTY0NzE3MDkiLCJzdWIiOiJBQzZiMmY3ZGEzZDk2YTY5ZTRiNTY1MmY3YjE5OGE0ODg5IiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoibHJub3ZvMTI0MTI0MTIzQGdtYWlsLmNvbSIsInZpZGVvIjp7InJvb20iOiJscm5vdm8xMjQxMjQxMjMifX19.CaEGkeAwbwwoTwsUSxj4TcL6UH4IKiUmfnVKnBKLD14","","","");
                        bean.setmId(c.getString("id"));
                        bean.setmName(c.getString("name"));
                        bean.setmEmail(c.getString("email"));
                        mlist.add(bean);

                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
            }

            return null;
        }
        protected void onPostExecute(String result) {
            recyclerView.setAdapter(new AccessTokenAdapter(this, mlist));
        }
    }
}
