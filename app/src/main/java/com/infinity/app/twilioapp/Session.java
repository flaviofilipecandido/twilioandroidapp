package com.infinity.app.twilioapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.infinity.app.twilioapp.view.InviteActivity;


public class Session extends AppCompatActivity {
    Button btn_login, btn_logout;
    Intent in;
    SharedPreferences sp;
    public static final String MyPREFERENCES = "login";
    public static final String Name = "Key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_logout = (Button) findViewById(R.id.btn_logout);

        if (PrefUtils.getFromPrefs(Session.this, "item", "").equals("1")) {
            Intent i = new Intent(getBaseContext(), InviteActivity.class);
            startActivity(i);
            finish();
        }
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String n = "1";
                PrefUtils.saveToPrefs(Session.this, "item", n);
                in = new Intent(Session.this, InviteActivity.class);
                startActivity(in);
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

