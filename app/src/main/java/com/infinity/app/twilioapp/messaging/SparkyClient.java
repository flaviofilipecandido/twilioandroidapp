package com.infinity.app.twilioapp.messaging;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to authenticate ourselves towards twilio via Sparky.
 */
public final class SparkyClient {

    private static final String TAG = "SparkyClient";


    /**
     * As of now access token do not have an expiration date, so we will need a strategy to re-query them on occasion
     */
    private AccessToken accessToken;
    private final SparkyConfiguration configuration;
    private static final String POST = "POST";
    private static final Gson gson = new Gson();
    private JsonElement jsonArray;

    /**
     * @param configuration the desired configuration for sparky.
     * @param loggedInUser  the logged in user with whom a twilio session will be started. This is an instance of {@link AccessToken} however it won't have the
     *                      token itself set (as that token is unknown by the time the client is instantiated). It will however contain the required user data
     *                      as specified by the class {@link AccessToken}.
     */
    SparkyClient(@NonNull SparkyConfiguration configuration, @NonNull AccessToken loggedInUser) {
        this.configuration = configuration;
        this.accessToken = loggedInUser;
    }

    /**
     * Unregisters the user from the sparky server.
     *
     * @param context the application context.
     */
    void unregisterFromServer(@NonNull Context context) {
        Ion.with(context).load(POST, getUnregisterUrl(context)).setJsonObjectBody(getAuthenticatedUser(accessToken)).asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            Log.i(TAG, "Session was ended - user has been removed from the server.");
                        } else {
                            Log.e(TAG, "Failed to end session - user has not been removed from the server", e);
                        }
                    }
                });
        Log.i(TAG, "Sent request to unregister user from server.");
    }

    /**
     * @param accessToken the access token used to construct the result value.
     * @return a json object based on the given access token.
     */
    @NonNull
    private JsonObject getAuthenticatedUser(@NonNull AccessToken accessToken) {
        return new JsonParser().parse(gson.toJson(accessToken)).getAsJsonObject();
    }

    /**
     * Requests all users from sparky, that are of the same account as the currently logged in user.
     *
     * @param context  application context.
     * @param callback the callback handling the result and notifying of success/failure.
     */
    void getUsers(@NonNull Context context, @NonNull final UserListCallback callback) {
        Log.i(TAG, "Requesting users from the server.");
        Ion.with(context)
                .load(getUserListUrl(context))
                .setStringBody(accessTokenToJson(accessToken))
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    /**
                     * @param e      not null if an error occurred; null otherwise
                     * @param result the resulting array of json objects (users)
                     */
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (e == null) {
                            e.printStackTrace();
                            Log.i(TAG, "Received a list of currently authenticated users");
                            List<AccessToken> userList = new ArrayList<>();
                            Type listType = new TypeToken<ArrayList<TwilioUser>>() {
                                
                            }.getType();
                            List<AccessToken> list = new Gson().fromJson(jsonArray, listType);
                            final int resultSize = result.size();
                            if (result != null && resultSize > 0) {
                                for (int i = 0; i < resultSize; i++) {
                                    JsonElement currentEntry = result.get(i);
                                    JsonObject jobject = currentEntry.getAsJsonObject();
                                    jobject = jobject.getAsJsonObject("result");
                                    JsonArray jarray = jobject.getAsJsonArray("result");
                                    userList.add(gson.fromJson(currentEntry, AccessToken.class));


                                }
                            }
                            //Notify subscribers
                            callback.onUserListReceived(userList);
                        } else {
                            //TODO the UI should handle this properly and show a message or not show the suggestion list at all
                            Log.e(TAG, "Failed to obtain a list of currently authenticated users. Providing an empty user list.", e);
                            callback.onFailedToGetUserList(e);
                        }
                    }
                });
    }

    /**
     * Sends an authentication request and notifies the callback upon success or failure.
     *
     * @param context  application context.
     * @param callback the callback will be notified of success or failure.
     */
    void authenticate(@NonNull final Context context, @NonNull final AuthCallback callback, boolean forceAuthentication) {
        Log.i(TAG, "Attempting to obtain access token");
        accessToken.setForceReAuthentication(forceAuthentication);
        Ion.with(context).load(POST, getAuthenticationUrl(context)).setLogging(TAG, Log.INFO)
                .setBodyParameter("Room", "apple")
                .setBodyParameter("Identity", "jd2@example.com")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.i(TAG, "Received response for access token request.");
                        boolean authSuccessful = false;
                        if (e == null) {
                            AccessToken newToken = gson.fromJson(result, AccessToken.class);
                            if (newToken != null && !TextUtils.isEmpty(newToken.getToken())) {
                                accessToken = newToken;
                                accessToken.setForceReAuthentication(false);
                                accessToken = new AccessToken("apple", accessToken.getToken(), "apple", "jd2@example.com", "apple");
                                authSuccessful = true;
                            }
                        }
                        if (authSuccessful) {
                            Log.i(TAG, String.format("Successfully obtained access token for %s.", accessToken.getUserName()));
                            callback.onAuthenticated(accessToken, context);

                        } else {
                            Log.e(TAG, "Failed to obtain access token.", e);
                            callback.onAuthenticationFailed();
                        }
                    }
                });
    }
    public AccessToken getAccessToken() {
        return accessToken;
    }

    /**
     * @return a json String that can be sent to sparky for authenticating yourself or requesting other data that requires you to be authenticated.
     */
    private static String accessTokenToJson(@NonNull AccessToken token) {
        return gson.toJson(token);
    }

    /**
     * @return URL to obtain all user currently registered on sparky for the same account as the logged in user.
     */
    @NonNull
    private String getUserListUrl(Context context) {
        return new StringBuilder(configuration.getServerUrl(context)).append(configuration.getPath(SparkyConfiguration.USER_LIST)).toString();
    }

    /**
     * @return URL used to request unregistering the current user from sparky.
     */
    @NonNull
    private String getUnregisterUrl(Context context) {
        return new StringBuilder(configuration.getServerUrl(context)).append(configuration.getPath(SparkyConfiguration.UNREGISTER)).toString();
    }

    /**
     * @return the URL used to request authentication from sparky. This will require an instance of {@link AccessToken} as a json object as the payload.
     */
    @NonNull
    private String getAuthenticationUrl(Context context) {
        return new StringBuilder(configuration.getServerUrl(context)).append(configuration.getPath(SparkyConfiguration.AUTHENTICATE)).toString();
    }

    /**
     * Implement this to receive authentication related callbacks.
     */
    interface AuthCallback {

        /**
         * Successfully obtained an authentication token.
         *
         * @param token   that contains relevant information regarding the token.
         * @param context the application context.
         */
        void onAuthenticated(AccessToken token, Context context);

        /**
         * Failed to obtain an authentication token.
         */
        void onAuthenticationFailed();
    }

    /**
     * Implement this to receive feedback related to obtaining a list of users.
     */
    interface UserListCallback {

        void onUserListReceived(List<AccessToken> users);

        /**
         * This means we failed to get an update on the userlist. You can either keep the list you already have or reset it.
         */
        void onFailedToGetUserList(@NonNull Exception error);
    }
}
