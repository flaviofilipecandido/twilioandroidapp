package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.twilio.video.LocalMedia;
import com.twilio.video.Participant;
import com.twilio.video.Room;

/**
 * Subscription used to stay up to date on room changes.
 */
public interface RoomStatusSubscription {

    /**
     * You successfully connected to the room you're subscribed for.
     */
    void onConnected(@NonNull Room room, @NonNull LocalMedia localMedia);

    /**
     * You failed to connect to the room.
     */
    void onConnectFailed();

    /**
     * You disconnected from the room.
     */
    void onDisconnected();

    /**
     * @param participant the user who joined your room.
     */
    void someoneJoined(@NonNull Participant participant);

    /**
     * @param participant the given user left the room.
     */
    void someoneLeft(@NonNull Participant participant);

    boolean doesRoomMatch(RoomStatus update);

    /**
     * @return true if incoming call notifications should be suppressed; false otherwise.
     */
    boolean shouldSilenceIncomingCallNotifications();

    abstract class Base implements RoomStatusSubscription {

        private final String roomNameFilter;

        /**
         * @param roomFilter we filter all incoming notifications by room, so we're not flooded with updates for rooms, that we don't care about.
         */
        protected Base(@Nullable AccessToken roomFilter) {
            this.roomNameFilter = roomFilter != null ? roomFilter.getCallerId() : "";
        }

        @Override
        public final boolean doesRoomMatch(RoomStatus update) {
            return update != null && update.getRoom() != null ? doesRoomMatch(update.getRoom().getName()) : false;
        }

        protected boolean doesRoomMatch(@Nullable String otherRoomName) {
            return roomNameFilter.equals(otherRoomName);
        }
    }
}
