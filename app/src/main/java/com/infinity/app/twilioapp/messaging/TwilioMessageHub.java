package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Message hub via which you can communicate with {@link TwilioVideoService} if available.
 */
public final class TwilioMessageHub implements MessageHub {

    private static volatile TwilioMessageHub instance;

    //TODO extract out an interface, rather than directly referencing the chat service
    private VideoService videoService;
    private OnlineContactsSubscriptionHandler onlineContactsSubscriptionHandler;
    private RoomStatusSubscriptionHandler roomChangedSubscriptionHandler;

    private TwilioMessageHub() {
    }

    /**
     * @return the only existing instance of this message hub.
     */
    public static TwilioMessageHub getInstance() {
        if (instance == null) {
            synchronized (TwilioMessageHub.class) {
                if (instance == null) {
                    instance = new TwilioMessageHub();
                }
            }
        }
        return instance;
    }

    void registerTwilioService(VideoService service) {
        videoService = service;
    }

    void unregisterTwilioService() {
        videoService = null;
    }

    public void subscribe(AvailableRoomsSubscription subscriber) {
        if (onlineContactsSubscriptionHandler == null) {
            onlineContactsSubscriptionHandler = new OnlineContactsSubscriptionHandler(this);
        }
        onlineContactsSubscriptionHandler.subscribe(subscriber);
    }

    public void unsubscribe(AvailableRoomsSubscription subscriber) {
        if (onlineContactsSubscriptionHandler != null) {
            onlineContactsSubscriptionHandler.unsubscribe(subscriber);
        }
    }

    public void subscribe(RoomStatusSubscription subscriber) {
        if (roomChangedSubscriptionHandler == null) {
            roomChangedSubscriptionHandler = new RoomStatusSubscriptionHandler(this);
        }
        roomChangedSubscriptionHandler.subscribe(subscriber);
    }

    public void unsubscribe(RoomStatusSubscription subscriber) {
        if (roomChangedSubscriptionHandler != null) {
            roomChangedSubscriptionHandler.unsubscribe(subscriber);
        }
    }

    void notifyRoomStatusChanged(@NonNull RoomStatus update) {
        if (roomChangedSubscriptionHandler != null) {
            roomChangedSubscriptionHandler.process(update);
        }
    }

    /**
     * Starts a call to the given room. The call was initiated by the local user.
     * This starts the Call Activity and asks the {@łink TwilioVideoService} to connect to the given room.
     *
     * @param onlineUser the room to join, if null is passed we'll join our own room.
     * @return true if the request was made; false if a problem occurred due to which the request could not be made (like the chat service not running).
     */
    public boolean onJoinRoom(@Nullable AccessToken onlineUser) {
        boolean result = hasVideoService();
        //We trigger the twilio service and ask it to join the given room
        if (result) {
            videoService.onJoinRoom(new JoinRoomRequest(onlineUser));
        }
        return result;
    }

    @Override
    public boolean hasVideoService() {
        return videoService != null;
    }

    @Nullable
    public VideoService getVideoService() {
        return videoService;
    }

    @Override
    public void onJoinOwnRoom(boolean disconnectFirst) {
        if (hasVideoService()) {
            videoService.joinOwnRoom(disconnectFirst);
        }
    }

    @Override
    public boolean shouldSilenceIncomingCallNotifications() {
        return roomChangedSubscriptionHandler != null ? roomChangedSubscriptionHandler.shouldSilenceIncomingCalls() : false;
    }
}
