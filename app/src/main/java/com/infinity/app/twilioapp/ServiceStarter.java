package com.infinity.app.twilioapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.infinity.app.twilioapp.messaging.TwilioVideoService;

public class ServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"service started", Toast.LENGTH_SHORT).show();

        Intent i = new Intent("com.coresystems.twiliovideo.messaging.TwilioVideoService");
        i.setClass(context, TwilioVideoService.class);
        context.startService(i);
    }
}