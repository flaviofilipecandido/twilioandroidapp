package com.infinity.app.twilioapp.messaging;

import android.util.Log;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Creates a sample twilio user.
 */
public final class TwilioUser {

    private static final String TAG = TwilioUser.class.getSimpleName();

    //All users belong to the same account
    private static final String accountName = "test-account";
    private static final String firstName;
    private static final String lastName;

    static {
        firstName = generateName(5);
        lastName = generateName(6);
        Log.i(TAG, String.format("Created twilio user %s %s", firstName, lastName));
    }

    private static String generateName(int length) {
        String value = RandomStringUtils.randomAlphabetic(length);
        value = value.toLowerCase();
        String rest = value.substring(1, value.length());
        String firstLetter = value.substring(0, 1).toUpperCase();
        return firstLetter + rest;
    }

    private TwilioUser() {
    }

    public static AccessToken toAccessToken() {
        return new AccessToken(firstName, "", String.format("%s %s", firstName, lastName), accountName);
    }
}
