package com.infinity.app.twilioapp.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.infinity.app.twilioapp.messaging.AccessToken;
import com.infinity.app.twilioapp.messaging.RoomStatusSubscription;
import com.twilio.video.LocalMedia;
import com.twilio.video.Participant;
import com.twilio.video.Room;

/**
 * Abstract base implementation, so interested parties can reduce their code to the event's they're actually interested in.
 */
abstract class DefaultRoomStatusSubscription extends RoomStatusSubscription.Base {

    private static final String TAG = DefaultRoomStatusSubscription.class.getSimpleName();

    /**
     * @param roomFilter we filter all incoming notifications by room name, so we're not flooded with updates for rooms, that we don't care about.
     */
    protected DefaultRoomStatusSubscription(@Nullable AccessToken roomFilter) {
        super(roomFilter);
    }

    @Override
    public void onConnected(@NonNull Room connectedTo, @NonNull LocalMedia roomsLocalMedia) {
        Log.i(TAG, "onConnected to room " + connectedTo.getName());
    }

    @Override
    public void onConnectFailed() {
        Log.i(TAG, "onConnectFailed");
    }

    @Override
    public void onDisconnected() {
        Log.i(TAG, "onDisconnected");
    }

    @Override
    public void someoneJoined(@NonNull Participant participant) {
        Log.i(TAG, "someoneJoined " + participant.getIdentity());
    }

    @Override
    public void someoneLeft(@NonNull Participant participant) {
        Log.i(TAG, "someoneLeft " + participant.getIdentity());
    }

    @Override
    public boolean shouldSilenceIncomingCallNotifications() {
        return false;
    }
}
