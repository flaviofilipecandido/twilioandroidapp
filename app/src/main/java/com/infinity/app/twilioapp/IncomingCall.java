package com.infinity.app.twilioapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.infinity.app.twilioapp.messaging.AccessToken;
import com.infinity.app.twilioapp.messaging.SparkyClient;
import com.infinity.app.twilioapp.view.CallActivity;

import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_OUTGOING_CALL;
import static com.infinity.app.twilioapp.view.CallActivity.BUNDLE_TARGET_ROOM;


public class IncomingCall extends AppCompatActivity {

    FloatingActionButton fab_accept_call, fab_reject_call;
    private SparkyClient sparkyClient;
    String newParticipant = null;
    private boolean isOutgoingCall;
    // The room we were meant to connect to
    private AccessToken targetRoom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.incoming_video_call);



        final Bundle extras = getIntent().getExtras();
        isOutgoingCall = extras.getBoolean(BUNDLE_OUTGOING_CALL);
        targetRoom = extras.getParcelable(BUNDLE_TARGET_ROOM);


        fab_accept_call = (FloatingActionButton) findViewById(R.id.fab_accept_call);
        fab_accept_call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(IncomingCall.this, CallActivity.class);
                intent.putExtra(BUNDLE_OUTGOING_CALL, isOutgoingCall);
                intent.putExtra(BUNDLE_TARGET_ROOM, targetRoom);
                startActivity(intent);
                finish();
            }


        });

        fab_reject_call = (FloatingActionButton) findViewById(R.id.fab_reject_call);
        fab_reject_call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();

            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }
}
