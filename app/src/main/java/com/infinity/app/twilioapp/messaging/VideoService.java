package com.infinity.app.twilioapp.messaging;

import android.support.annotation.NonNull;

/**
 * Interface for a chat service as used by {@link TwilioMessageHub}.
 */
interface VideoService {

    /**
     * Requests a list of users that are currently active on the same account that the logged in user is.
     *
     * @param callback will be notified once the result has been retrieved.
     */
    void getUsers(@NonNull final SparkyClient.UserListCallback callback);

    /**
     * Called if someone wants to join a given room.
     */
    void onJoinRoom(@NonNull JoinRoomRequest event);

    /**
     * Requests a room update which will be delivered to {@link TwilioMessageHub#notifyRoomStatusChanged} and from there will be broadcasted to all
     * subscribers.
     */
    void requestRoomUpdate();

    /**
     * Joins our own room so we can wait for incoming calls.
     *
     * @param disconnectFirst true if we should disconnect from the current room first; false otherwise.
     */
    void joinOwnRoom(boolean disconnectFirst);
}
