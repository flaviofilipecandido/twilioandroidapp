package com.infinity.app.twilioapp.messaging;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.infinity.app.twilioapp.MainActivity;

import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Periodically retrieves room-updates, i.e. who is currently online.
 */
final class OnlineContactsSubscriptionHandler extends SubscriptionHandler<AvailableRoomsSubscription> {

    private static final long UPDATE_INTERVAL = 15000;
    private Timer availableRoomsUpdateTimer;
    private final MessageHub messageHub;

    OnlineContactsSubscriptionHandler(MessageHub hub) {
        messageHub = hub;
    }

    @Override
    void subscribe(@Nullable AvailableRoomsSubscription subscription) {
        super.subscribe(subscription);
        //We definitely know the list has at least one element, so start producing room updates
        if (availableRoomsUpdateTimer == null) {
            availableRoomsUpdateTimer = new Timer();
            //We'll query for who is online in regular intervals
            availableRoomsUpdateTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (messageHub.hasVideoService()) {
                        messageHub.getVideoService().getUsers(new SparkyClient.UserListCallback() {

                            @Override
                            public void onUserListReceived(List<AccessToken> users) {
                                //Post to main thread
                                new Handler(Looper.getMainLooper()).post(new OnRoomsUpdatedTask(null, users, getSubscribers()));
                            }

                            @Override
                            public void onFailedToGetUserList(@NonNull Exception error) {
                                //Post to main thread
                                new Handler(Looper.getMainLooper()).post(new OnRoomsUpdatedTask(error, null, getSubscribers()));
                            }
                        });
                    } else {
                        //Try starting the chat service again
                        TwilioVideoService.startTwilioService(MainActivity.getContext());
                    }
                }
            }, 0, UPDATE_INTERVAL);
        }
    }

    @Override
    int unsubscribe(@Nullable AvailableRoomsSubscription subscription) {
        int subscriberCount = super.unsubscribe(subscription);
        if (subscriberCount == 0 && availableRoomsUpdateTimer != null) {
            //Stop the timer, we don't need to keep pulling updates if no one cares
            availableRoomsUpdateTimer.cancel();
            availableRoomsUpdateTimer = null;
        }
        return subscriberCount;
    }

    private static class OnRoomsUpdatedTask implements Runnable {

        private final Exception error;
        private final List<AccessToken> users;
        private final Collection<AvailableRoomsSubscription> subscriptions;

        private OnRoomsUpdatedTask(@Nullable Exception error, @Nullable List<AccessToken> users,
                                   @Nullable Collection<AvailableRoomsSubscription> subscriptions) {
            this.users = users;
            this.subscriptions = subscriptions;
            this.error = error;
        }

        @Override
        public void run() {
            if (subscriptions != null) {
                for (AvailableRoomsSubscription sub : subscriptions) {
                    if (error != null) {
                        //TODO change this to a more user-friendly message
                        sub.onError(error.getMessage());
                    } else {
                        sub.onUpdate(users);
                    }
                }
            }
        }
    }

}
