package com.infinity.app.twilioapp;

import android.app.Application;
import android.content.Context;

public class MainActivity extends Application {



    private static Context self;

    @Override
    public void onCreate() {
        super.onCreate();
        self = this;
    }

    public static Context getContext() {
        return self;
    }
}