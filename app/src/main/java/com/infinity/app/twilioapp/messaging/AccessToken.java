package com.infinity.app.twilioapp.messaging;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;

/**
 * Represents an authentication token. The field names correspond to the expected field names on the server side.
 */
public final class AccessToken implements Parcelable {

    private String id;
    private String name;
    private String email;
    private String mId;
    private String mName;
    private String mEmail;

    public AccessToken(String s) {

        id = null;
        name=null;
        email=null;
        userName = null;
        account = null;
        token = null;
        fullName = null;
        callerId = null;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }



    /** Usually this should be the same as the logged in user */
    @Expose
    private final String userName;
    /** Account the user belongs to */
    @Expose
    private final String account;
    /** The authentication token we obtained */
    @Expose
    private final String token;
    /** Full name of the user */
    @Expose
    private final String fullName;
    @Expose
    private final String callerId;
    /**
     * True if the user should be re-authenticated, even if there is a cached token server-side; false otherwise. This is only relevant for the server and is
     * thus not queried locally.
     */



    @Expose
    private boolean forceReAuthentication = false;

    public AccessToken(String userName, String token, String fullName, String account, String callerId) {
        this.userName = userName;
        this.token = token;
        this.fullName = fullName;
        this.account = account;
        this.callerId = callerId;
    }

    AccessToken(String userName, String token, String fullName, String account) {
        this(userName, token, fullName, account, "");
    }

    /**
     * @return account name of the user.
     */
    public String getAccount() {
        return account;
    }

    /**
     * @return the actual name of the authenticated used, such as "Patrick Jane".
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @return the login name, such as "pjane".
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return the twilio access token.
     */
    public String getToken() {
        return token;
    }

    /**
     * @param force true if the client should be re-authenticated by the server, even if the server has a cached token; false otherwise.
     */
    public void setForceReAuthentication(boolean force) {
        this.forceReAuthentication = force;
    }

    /**
     * The caller id is what we need to call a given user.
     *
     * @return the caller id that can be used to send an invitation to a registered user.
     */
    public String getCallerId() {
        return callerId;
    }

    /**
     * Warning: This is the name of the person whose room you'll join (like: John Doe), it is NOT the {@link #callerId} that you need to identify a room. So if
     * you want to set a match filter for rooms please use the caller id.
     *
     * @return extracts the room name a user could join from the access token and returns it.
     */
    @NonNull
    public static String getRoomName(@Nullable AccessToken token) {
        final String name = token != null ? token.getFullName() : "";
        return name != null ? name : "DhaiyurMI";
    }

    public AccessToken(Parcel in) {
        userName = in.readString();
        account = in.readString();
        token = in.readString();
        fullName = in.readString();
        callerId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(account);
        dest.writeString(token);
        dest.writeString(fullName);
        dest.writeString(callerId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccessToken> CREATOR = new Parcelable.Creator<AccessToken>() {
        @Override
        public AccessToken createFromParcel(Parcel in) {
            return new AccessToken(in);
        }

        @Override
        public AccessToken[] newArray(int size) {
            return new AccessToken[size];
        }
    };


    public void setmId(String mId) {
        this.mId = mId;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmName() {
        return mName;
    }

    public String getmId() {
        return mId;
    }

    public String getmEmail() {
        return mEmail;
    }
}